public class Board{
	//fields
	private Tile[][] grid; //toString will be a loop
	private final int boardSize;
	//constructor
	public Board(){
		this.boardSize = 3;
		grid = new Tile[this.boardSize][this.boardSize];
		for(int i = 0; i < grid.length; i++){
			for(int j = 0; j < this.boardSize; j++){
				grid[i][j] = Tile.BLANK;
			}
		}
	}
	//toString Override to print TicTakToe table
	public String toString(){
		String output = "";
		for(int i = 0; i < grid.length; i++){
			for(int j = 0; j < this.boardSize; j++){
				output += grid[i][j].getName() + " ";
			}
			output += "\n";
		}
		return output;
	}
	//Method that checks if token is allowed to be place & replaces blank if true
	public boolean placeToken(int row, int col, Tile playerToken){
		if((col >= this.boardSize && col <= 0) || (row >= this.boardSize && row <= 0)){
			return false;
		}
		else if(grid[col][row] == Tile.BLANK){
			grid[col][row] = playerToken;
			return true;
		}
		return false;
	}
	//Method that checks entire board for blanks
	public boolean checkIfFull(){
		for(int i = 0; i < grid.length; i++){
			for(int j = 0; j < this.boardSize; j++){
				if(grid[i][j] == Tile.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	//Used for checking hoirizontal win condition
	private boolean checkIfWinningHorizontal(Tile playerToken){
		int tokenCounter = 0;
		for(int i = 0; i < grid.length; i++){
			for(int j = 0; j < this.boardSize; j++){
				if(grid[i][j] == playerToken){
					tokenCounter++;
				}
				else{
					tokenCounter = 0;
				}
			}
			if(tokenCounter == 3){
				return true;
			}
		}
		return false;	
	}
	//Used to check vertical win condition
	private boolean checkIfWinningVertical(Tile playerToken){
		int tokenCounter = 0;
		for(int i = 0; i < grid.length; i++){
			for(int j = 0; j < this.boardSize; j++){
				if(grid[j][i] == playerToken){
					tokenCounter++;
				}
				else{
					tokenCounter = 0;
				}
			}
			if(tokenCounter == 3){
				return true;
			}
		}
		return false;
	}
	//Checks if winner using previous win condtitons (called in application)
	public boolean checkIfWinning(Tile playerToken){
		if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken)){
			return true;
		}
		return false;
	}
}