import java.util.Scanner;
public class TicTacToeApp{
	public static void main(String[] args){
		//Calls helper method
		runGame();
	}
	//Checks player int to check who's turn it is in the game and switches to the associated token
	public static Tile playerRotation(Tile playerToken, int player){
		if(player == 1){
			playerToken = Tile.X;
		}
		else{
			playerToken = Tile.O;
		}
		return playerToken;
	}
	//public static boolean winCondition(Tile playerToken, int player, Board gameBoard){
		
	//}
	//Main game method that contains the loop that will run till game over
	public static void runGame(){
		Scanner reader = new Scanner(System.in);
		Board gameBoard = new Board();
		boolean gameOver = false;
		int player = 1;
		//Player1 = X & Player2 = O
		Tile playerToken = Tile.X;
		
		System.out.println("Welcome to TicTacToe!");
		System.out.println("Player 1's Token: " + Tile.X);
		System.out.println("Player 2's Token: " + Tile.O);
		
		//Tile currentToken = playerRotation(playerToken, player); //why does this not change current token?
		
		while(!gameOver){
			System.out.println(gameBoard);
			System.out.println("Player " + player + "'s turn");
			System.out.println("Please input the row and column you and to place you token ex: Row: 1 Column: 2");
			System.out.print("Row: "); //Between 0 & gameBoard length
			int choosenRow = Integer.parseInt(reader.nextLine()); //Between 0 & gameBoard length
			System.out.print("Column: ");
			int choosenColumn = Integer.parseInt(reader.nextLine());
			
			boolean isBlankSpace = gameBoard.placeToken(choosenRow, choosenColumn, playerRotation(playerToken, player));
			
			//Checks to see if player is placing a token on a blank space and places it
			if(isBlankSpace){
				gameBoard.placeToken(choosenRow, choosenColumn, playerRotation(playerToken, player));
			}
			else{
				//Prints when attemped token is trying to be placed in a non blank line
				System.out.println("Occupied space, Enter different inputs");
			}
			/* Depending on the size of the board, if the same tokens line up horizonalily, 
			vertically the player w/ the associated token wins */
			if(gameBoard.checkIfWinning(playerToken)){
				System.out.println("Player: " + player + " Wins!");
				gameOver= true;
			}
			//Board is full when the checkIfWinning condition is not fufilled & there are no empty tiles
			else if(gameBoard.checkIfFull()){
				System.out.println("It's a tie!");
				gameOver = true;
			}
			//If no winner or board is not full, then switch playerToken
			else{
				player += 1;
				if(player > 2){
					player = 1;
				}
			}
			//gameOver = winCondition(playerToken, player, gameBoard);
		}
	}
}